import { Outlet, NavLink } from 'react-router-dom';

function Nav() {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
                <NavLink to="/" className="navbar-brand">Conference GO!</NavLink>
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                    <li className="nav-item">
                        <NavLink to="/" className="nav-link" aria-current="page">Home</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="locations/new" className="nav-link" id="create-location" aria-current="page">New location</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/conferences/new" className="nav-link" id="create-conference" aria-current="page">New conference</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/presentations/new" className="nav-link" id="create-presentation" aria-current="page">New presentation</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/attendees/new" className="nav-link" id="create-attendee" aria-current="page">Signup</NavLink>
                    </li>
                </ul>
            </div>
        </nav>
    );
}

export default Nav;
